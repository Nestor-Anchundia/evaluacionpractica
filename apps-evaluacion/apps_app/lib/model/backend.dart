


import 'package:usuario_app/model/apps.dart';

class Backend{
  static final Backend _backend = Backend._internal();

  factory Backend(){
    return _backend;
  }

  Backend._internal();

  final _apps = [
    
    App(
      id: 1,
      nombre: "movies app",
      plataforma: "Android"),

      App(
      id: 2,
      nombre: "music app",
      plataforma: "Android")
,

App(
      id: 3,
      nombre: "books app",
      plataforma: "Android")
,

App(
      id: 4,
      nombre: "movies app",
      plataforma: "Android")


  ];


  List <App> getApps(){
    return _apps;
  }

  void markAppComoVisto(int id){
    var indice= _apps.indexWhere((app) => app.id==id);
    _apps[indice].revisado=true;
  }

  void deleteApp(int id){
    _apps.removeWhere((app) =>app.id==id );
  }

}