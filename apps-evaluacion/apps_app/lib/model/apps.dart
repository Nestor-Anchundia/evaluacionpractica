

class App {
  final int id;
  final String nombre;
  final String plataforma;
  bool revisado;

App({
  required this.id,
  required this.nombre,
  required this.plataforma,
  this.revisado=false
});

}