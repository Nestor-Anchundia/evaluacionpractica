


import 'package:flutter/material.dart';
import 'package:usuario_app/model/apps.dart';

class AppDetail extends StatelessWidget{
 final App app;
const AppDetail({Key?key, required this.app}):super(key: key);

@override
Widget build (BuildContext context){

  return Scaffold(
    appBar:  AppBar(title: Text("consulta individual de Apps")),
    body: Container(
      padding:const EdgeInsets.all(10),
      child: Column(children: [
       Text("Nombre: ${app.nombre}"),
       Text("Plataforma: ${app.plataforma}"),
       Text("Id: ${app.id}")
      ]),
    ),
  );
}

}