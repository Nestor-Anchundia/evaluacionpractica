

import 'package:flutter/material.dart';
import 'package:usuario_app/model/apps.dart';
import 'package:usuario_app/model/backend.dart';
import 'package:usuario_app/screens/appsdetail.dart';
import 'package:usuario_app/widgets/appswidget.dart';

class AppList extends StatefulWidget{
  final String titulo;

  const AppList({Key?key, required this.titulo }):super(key: key);

    @override
   _AppListState createState()=> _AppListState();

}

class _AppListState extends State <AppList>{


var apps = Backend().getApps();

void markAppComoVisto(int id){
  Backend().markAppComoVisto(id);
  setState(() {
   apps =Backend().getApps();
});
}


void deleteApp(int id){
  Backend().deleteApp(id);
  setState(() {
    apps = Backend().getApps();
  });
}

void showDetail(App app){
  Navigator.push(context, MaterialPageRoute(builder: (context){
    return AppDetail(app: app);
  }));
  
    Backend().markAppComoVisto(app.id);
    setState(() {
      apps=Backend().getApps();
    });
}


@override
Widget build (BuildContext context){

  return Scaffold(
    appBar: AppBar(title: Text(widget.titulo)),
    body: ListView.separated(itemBuilder: (BuildContext context, int index)=>AppsWidget(
      app: apps[index],
      onTap: showDetail,
      onSwipe: deleteApp,
      onLongPress: markAppComoVisto),
    separatorBuilder: (BuildContext context, int index)=> const Divider(
      color: Colors.black,
      thickness: 4,
    ),
    itemCount: apps.length)
  );
}

}