


import 'package:flutter/material.dart';
import 'package:usuario_app/model/apps.dart';

class AppsWidget extends StatelessWidget{

   final App app;
  final Function onTap;
  final Function onSwipe;
  final Function onLongPress;

  const AppsWidget(
      {Key? key,
      required this.app,
      required this.onTap,
      required this.onSwipe,
      required this.onLongPress})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragEnd: (DragEndDetails details) {
        onSwipe(app.id);
      },
      onLongPress: () {
        onLongPress(app.id);
      },
      onTap: () {
        onTap(app);
      },
      child: Container(
        padding: const EdgeInsets.all(10.0),
        height: 80.0,
        child: Row(
          children: [
            Expanded(
              flex: 1,
              child: Container(
                height: 12.0,
                decoration: BoxDecoration(
                    color: app.revisado ? Colors.transparent : Colors.amber,
                    shape: BoxShape.circle),
              ),
            ),
            Expanded(
              flex: 9,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  
                  
                  Text("Nombre: ${app.nombre}"),
                  Text("Plataforma: ${app.plataforma}"),
                  Text("Id: ${app.id.toString()}")
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

